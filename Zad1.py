import numpy as np
import matplotlib.pyplot as plt


def adaline(input_list, weights, epochs, alpha, activ, acceptable_error=0.1):
    i = 0
    result = []
    for x in range(len(input_list)):
        result.append(0)
    for y in range(epochs):
        for x in range(len(input_list)):
            result[x] = input_list[x][0] @ weights
            error = input_list[x][1] - result[x]
            if input_list[x][1] - activ(0, result[x]):
                i = i + 1
            if error ** 2 > acceptable_error:
                weights = weights + alpha * error * input_list[x][0]

    for x in range(len(input_list)):
        result[x] = activ(0, result[x])
    return weights, result, i


def perceptron(input_list, weights, epochs, alpha, activ, activation=0.5):
    i = 0
    result = []
    for x in range(len(input_list)):
        result.append(0)
    errors = []
    for y in range(epochs):
        epoch_error = []
        for x in range(len(input_list)):
            net = input_list[x][0] @ weights
            result[x] = activ(activation, net)
            error = input_list[x][1] - result[x]
            epoch_error.append(error)
            if not error == 0:
                weights = weights + alpha * error * input_list[x][0]
                i = i + 1
        errors.append(np.sum(np.abs(epoch_error)) / len(epoch_error))
    return weights, result, i


def binary(activation, net):
    if net > activation:
        return 1
    else:
        return 0


def bipolar(activation, net):
    if net > activation:
        return 1
    else:
        return -1


def list_binary_or_generate():
    listBinaryOR = [
        [np.array([1, 0, 0]), 0],
        [np.array([1, 0, 1]), 1],
        [np.array([1, 1, 0]), 1],
        [np.array([1, 1, 1]), 1]
    ]
    return listBinaryOR


def list_bipolar_or_generate():
    listBipolarOR = [
        [np.array([1, -1, -1]), -1],
        [np.array([1, -1, 1]), 1],
        [np.array([1, 1, -1]), 1],
        [np.array([1, 1, 1]), 1]
    ]
    return listBipolarOR


def list_binary_and_generate():
    listBinaryAND = [
        [np.array([1, 0, 0]), 0],
        [np.array([1, 0, 1]), 0],
        [np.array([1, 1, 0]), 0],
        [np.array([1, 1, 1]), 1]
    ]
    return listBinaryAND


def list_bipolar_and_generate():
    listBipolarAND = [
        [np.array([1, -1, -1]), -1],
        [np.array([1, -1, 1]), -1],
        [np.array([1, 1, -1]), -1],
        [np.array([1, 1, 1]), 1]
    ]
    return listBipolarAND


def learn(weight_low, weight_high, alpha, activation, acceptable_error):
    global binaryOrPerceptron, binaryOrPerceptronResult, bipolarOrPerceptronResult, binaryAndPerceptronResult, bipolarAndPerceptronResult, adalineOr, adalineOrResult, adalineAndResult
    weight = np.random.uniform(weight_low, weight_high, 3)
    binaryOrPerceptron = perceptron(listBinaryOR, weight, 100, alpha, binary, activation)
    binaryOrPerceptronResult = binaryOrPerceptronResult + binaryOrPerceptron[2]
    weight = np.random.uniform(weight_low, weight_high, 3)
    bipolarOrPerceptron = perceptron(listBipolarOR, weight, 100, alpha, bipolar, activation)
    bipolarOrPerceptronResult = bipolarOrPerceptronResult + bipolarOrPerceptron[2]
    weight = np.random.uniform(weight_low, weight_high, 3)
    binaryAndPerceptron = perceptron(listBinaryAND, weight, 100, alpha, binary, activation)
    binaryAndPerceptronResult = binaryAndPerceptronResult + binaryAndPerceptron[2]
    weight = np.random.uniform(weight_low, weight_high, 3)
    bipolarAndPerceptron = perceptron(listBipolarAND, weight, 100, alpha, bipolar, activation)
    bipolarAndPerceptronResult = bipolarAndPerceptronResult + bipolarAndPerceptron[2]
    weight = np.random.uniform(weight_low, weight_high, 3)
    adalineOr = adaline(listBipolarOR, weight, 100, alpha, bipolar, acceptable_error)
    adalineOrResult = adalineOrResult + adalineOr[2]
    weight = np.random.uniform(weight_low, weight_high, 3)
    adalineAnd = adaline(listBipolarAND, weight, 100, alpha, bipolar, acceptable_error)
    adalineAndResult = adalineAndResult + adalineAnd[2]


def test(weight_low, weight_high, alpha=0.2, activation=0.5, acceptable_error=0.1):
    global binaryOrPerceptronResult, bipolarOrPerceptronResult, binaryAndPerceptronResult, bipolarAndPerceptronResult, adalineOrResult, adalineAndResult
    binaryOrPerceptronResult = 0
    bipolarOrPerceptronResult = 0
    binaryAndPerceptronResult = 0
    bipolarAndPerceptronResult = 0
    adalineOrResult = 0
    adalineAndResult = 0
    seed = 122
    np.random.seed(seed)
    for x in range(10):
        learn(weight_low, weight_high, alpha, activation, acceptable_error)
        seed = seed + x
        np.random.seed(seed + x)
    binaryOrPerceptronResult = binaryOrPerceptronResult / 10
    bipolarOrPerceptronResult = bipolarOrPerceptronResult / 10
    binaryAndPerceptronResult = binaryAndPerceptronResult / 10
    bipolarAndPerceptronResult = bipolarAndPerceptronResult / 10
    adalineOrResult = adalineOrResult / 10
    adalineAndResult = adalineAndResult / 10


def const_alpha():
    global binaryOrPerceptronArrayResult, bipolarOrPerceptronArrayResult, binaryAndPerceptronArrayResult, bipolarAndPerceptronArrayResult, adalineOrArrayResult, adalineAndArrayResult
    binaryOrPerceptronArrayResult = []
    bipolarOrPerceptronArrayResult = []
    binaryAndPerceptronArrayResult = []
    bipolarAndPerceptronArrayResult = []
    adalineOrArrayResult = []
    adalineAndArrayResult = []
    test(-1, 1)
    binaryOrPerceptronArrayResult.append(binaryOrPerceptronResult)
    bipolarOrPerceptronArrayResult.append(bipolarOrPerceptronResult)
    binaryAndPerceptronArrayResult.append(binaryAndPerceptronResult)
    bipolarAndPerceptronArrayResult.append(bipolarAndPerceptronResult)
    adalineOrArrayResult.append(adalineOrResult)
    adalineAndArrayResult.append(adalineAndResult)
    test(-0.8, 0.8)
    binaryOrPerceptronArrayResult.append(binaryOrPerceptronResult)
    bipolarOrPerceptronArrayResult.append(bipolarOrPerceptronResult)
    binaryAndPerceptronArrayResult.append(binaryAndPerceptronResult)
    bipolarAndPerceptronArrayResult.append(bipolarAndPerceptronResult)
    adalineOrArrayResult.append(adalineOrResult)
    adalineAndArrayResult.append(adalineAndResult)
    test(-0.6, 0.6)
    binaryOrPerceptronArrayResult.append(binaryOrPerceptronResult)
    bipolarOrPerceptronArrayResult.append(bipolarOrPerceptronResult)
    binaryAndPerceptronArrayResult.append(binaryAndPerceptronResult)
    bipolarAndPerceptronArrayResult.append(bipolarAndPerceptronResult)
    adalineOrArrayResult.append(adalineOrResult)
    adalineAndArrayResult.append(adalineAndResult)
    test(-0.4, 0.4)
    binaryOrPerceptronArrayResult.append(binaryOrPerceptronResult)
    bipolarOrPerceptronArrayResult.append(bipolarOrPerceptronResult)
    binaryAndPerceptronArrayResult.append(binaryAndPerceptronResult)
    bipolarAndPerceptronArrayResult.append(bipolarAndPerceptronResult)
    adalineOrArrayResult.append(adalineOrResult)
    adalineAndArrayResult.append(adalineAndResult)
    test(-0.2, 0.2)
    binaryOrPerceptronArrayResult.append(binaryOrPerceptronResult)
    bipolarOrPerceptronArrayResult.append(bipolarOrPerceptronResult)
    binaryAndPerceptronArrayResult.append(binaryAndPerceptronResult)
    bipolarAndPerceptronArrayResult.append(bipolarAndPerceptronResult)
    adalineOrArrayResult.append(adalineOrResult)
    adalineAndArrayResult.append(adalineAndResult)
    plt.plot([1, 0.8, 0.6, 0.4, 0.2], binaryOrPerceptronArrayResult, 'r')
    plt.plot([1, 0.8, 0.6, 0.4, 0.2], bipolarOrPerceptronArrayResult, 'g')
    plt.plot([1, 0.8, 0.6, 0.4, 0.2], adalineOrArrayResult, 'b')
    plt.legend(['Binarny perceptron [parametr aktywacji > 0.5]', 'Bipolarny perceptron [parametr aktywacji > 0.5]',
                'Aadaline [akceptowany blad = 0.1]'])
    plt.xlabel('wielkosc poczatkowa generowanych wag')
    plt.ylabel('srednia liczba epok')
    plt.title('OR dla alpha = 0.2')
    plt.axis([0.2, 1, 0, 15])
    plt.show()
    plt.plot([1, 0.8, 0.6, 0.4, 0.2], binaryAndPerceptronArrayResult, 'r')
    plt.plot([1, 0.8, 0.6, 0.4, 0.2], bipolarAndPerceptronArrayResult, 'g')
    plt.plot([1, 0.8, 0.6, 0.4, 0.2], adalineAndArrayResult, 'b')
    plt.legend(['Binarny perceptron [parametr aktywacji > 0.5]', 'Bipolarny perceptron [parametr aktywacji > 0.5]',
                'Aadaline [akceptowany blad = 0.1]'])
    plt.xlabel('wielkosc poczatkowa generowanych wag')
    plt.ylabel('srednia liczba epok')
    plt.title('AND dla alpha = 0.2')
    plt.axis([0.2, 1, 0, 15])
    plt.show()


def const_weight():
    global binaryOrPerceptronArrayResult, bipolarOrPerceptronArrayResult, binaryAndPerceptronArrayResult, bipolarAndPerceptronArrayResult, adalineOrArrayResult, adalineAndArrayResult
    binaryOrPerceptronArrayResult = []
    bipolarOrPerceptronArrayResult = []
    binaryAndPerceptronArrayResult = []
    bipolarAndPerceptronArrayResult = []
    adalineOrArrayResult = []
    adalineAndArrayResult = []
    test(-0.5, 0.5, 0.8)
    binaryOrPerceptronArrayResult.append(binaryOrPerceptronResult)
    bipolarOrPerceptronArrayResult.append(bipolarOrPerceptronResult)
    binaryAndPerceptronArrayResult.append(binaryAndPerceptronResult)
    bipolarAndPerceptronArrayResult.append(bipolarAndPerceptronResult)
    adalineOrArrayResult.append(adalineOrResult)
    adalineAndArrayResult.append(adalineAndResult)
    test(-0.5, 0.5, 0.6)
    binaryOrPerceptronArrayResult.append(binaryOrPerceptronResult)
    bipolarOrPerceptronArrayResult.append(bipolarOrPerceptronResult)
    binaryAndPerceptronArrayResult.append(binaryAndPerceptronResult)
    bipolarAndPerceptronArrayResult.append(bipolarAndPerceptronResult)
    adalineOrArrayResult.append(adalineOrResult)
    adalineAndArrayResult.append(adalineAndResult)
    test(-0.5, 0.5, 0.4)
    binaryOrPerceptronArrayResult.append(binaryOrPerceptronResult)
    bipolarOrPerceptronArrayResult.append(bipolarOrPerceptronResult)
    binaryAndPerceptronArrayResult.append(binaryAndPerceptronResult)
    bipolarAndPerceptronArrayResult.append(bipolarAndPerceptronResult)
    adalineOrArrayResult.append(adalineOrResult)
    adalineAndArrayResult.append(adalineAndResult)
    test(-0.5, 0.5, 0.2)
    binaryOrPerceptronArrayResult.append(binaryOrPerceptronResult)
    bipolarOrPerceptronArrayResult.append(bipolarOrPerceptronResult)
    binaryAndPerceptronArrayResult.append(binaryAndPerceptronResult)
    bipolarAndPerceptronArrayResult.append(bipolarAndPerceptronResult)
    adalineOrArrayResult.append(adalineOrResult)
    adalineAndArrayResult.append(adalineAndResult)
    plt.plot([0.8, 0.6, 0.4, 0.2], binaryOrPerceptronArrayResult, 'r')
    plt.plot([0.8, 0.6, 0.4, 0.2], bipolarOrPerceptronArrayResult, 'g')
    plt.plot([0.8, 0.6, 0.4, 0.2], adalineOrArrayResult, 'b')
    plt.legend(['Binarny perceptron', 'Bipolarny perceptron', 'Aadaline'])
    plt.xlabel('wielkosc alpha')
    plt.ylabel('srednia liczba epok')
    plt.title('OR dla wag = [-0.5, 0.5]')
    plt.axis([0.2, 0.8, 0, 15])
    plt.show()
    plt.plot([0.8, 0.6, 0.4, 0.2], binaryAndPerceptronArrayResult, 'r')
    plt.plot([0.8, 0.6, 0.4, 0.2], bipolarAndPerceptronArrayResult, 'g')
    plt.plot([0.8, 0.6, 0.4, 0.2], adalineAndArrayResult, 'b')
    plt.legend(['Binarny perceptron', 'Bipolarny perceptron', 'Aadaline'])
    plt.xlabel('wielkosc alpha')
    plt.ylabel('srednia liczba epok')
    plt.title('AND dla wag = [-0.5, 0.5]')
    plt.axis([0.2, 0.8, 0, 15])
    plt.show()


def change_activ_acepptable():
    global binaryOrPerceptronArrayResult, bipolarOrPerceptronArrayResult, binaryAndPerceptronArrayResult, bipolarAndPerceptronArrayResult, adalineOrArrayResult, adalineAndArrayResult
    binaryOrPerceptronArrayResult = []
    bipolarOrPerceptronArrayResult = []
    binaryAndPerceptronArrayResult = []
    bipolarAndPerceptronArrayResult = []
    adalineOrArrayResult = []
    adalineAndArrayResult = []
    test(-0.5, 0.5, 0.2, 0.8, 0.8)
    binaryOrPerceptronArrayResult.append(binaryOrPerceptronResult)
    bipolarOrPerceptronArrayResult.append(bipolarOrPerceptronResult)
    binaryAndPerceptronArrayResult.append(binaryAndPerceptronResult)
    bipolarAndPerceptronArrayResult.append(bipolarAndPerceptronResult)
    adalineOrArrayResult.append(adalineOrResult)
    adalineAndArrayResult.append(adalineAndResult)
    test(-0.5, 0.5, 0.2, 0.6, 0.6)
    binaryOrPerceptronArrayResult.append(binaryOrPerceptronResult)
    bipolarOrPerceptronArrayResult.append(bipolarOrPerceptronResult)
    binaryAndPerceptronArrayResult.append(binaryAndPerceptronResult)
    bipolarAndPerceptronArrayResult.append(bipolarAndPerceptronResult)
    adalineOrArrayResult.append(adalineOrResult)
    adalineAndArrayResult.append(adalineAndResult)
    test(-0.5, 0.5, 0.2, 0.4, 0.4)
    binaryOrPerceptronArrayResult.append(binaryOrPerceptronResult)
    bipolarOrPerceptronArrayResult.append(bipolarOrPerceptronResult)
    binaryAndPerceptronArrayResult.append(binaryAndPerceptronResult)
    bipolarAndPerceptronArrayResult.append(bipolarAndPerceptronResult)
    adalineOrArrayResult.append(adalineOrResult)
    adalineAndArrayResult.append(adalineAndResult)
    test(-0.5, 0.5, 0.2, 0.2, 0.2)
    binaryOrPerceptronArrayResult.append(binaryOrPerceptronResult)
    bipolarOrPerceptronArrayResult.append(bipolarOrPerceptronResult)
    binaryAndPerceptronArrayResult.append(binaryAndPerceptronResult)
    bipolarAndPerceptronArrayResult.append(bipolarAndPerceptronResult)
    adalineOrArrayResult.append(adalineOrResult)
    adalineAndArrayResult.append(adalineAndResult)
    plt.plot([0.8, 0.6, 0.4, 0.2], binaryOrPerceptronArrayResult, 'r')
    plt.plot([0.8, 0.6, 0.4, 0.2], bipolarOrPerceptronArrayResult, 'g')
    plt.plot([0.8, 0.6, 0.4, 0.2], binaryAndPerceptronArrayResult, 'b')
    plt.plot([0.8, 0.6, 0.4, 0.2], bipolarAndPerceptronArrayResult, 'k')
    plt.legend(['Binarny OR', 'Bipolarny OR', 'Binarny AND', 'Bipolarny AND'])
    plt.xlabel('wartosc parametru funkcji aktywacji')
    plt.ylabel('srednia liczba epok')
    plt.title('Perceptron dla wag = [-0.5, 0.5], alpha = 0.2')
    plt.axis([0.2, 0.8, 0, 15])
    plt.show()
    plt.plot([0.8, 0.6, 0.4, 0.2], adalineOrArrayResult, 'r')
    plt.plot([0.8, 0.6, 0.4, 0.2], adalineOrArrayResult, 'g')
    plt.legend(['OR', 'AND'])
    plt.xlabel('wartosc parametru funkcji aktywacji')
    plt.ylabel('srednia liczba epok')
    plt.title('Adaline dla wag = [-0.5, 0.5], alpha = 0.2')
    plt.axis([0.2, 0.8, 0, 15])
    plt.show()


if __name__ == "__main__":
    # listBinaryOR = list_binary_or_generate()
    # listBipolarOR = list_bipolar_or_generate()
    # listBinaryAND = list_binary_and_generate()
    # listBipolarAND = list_bipolar_and_generate()
    # 
    # np.random.seed(122)
    # 
    # print('Perceptron OR:')
    # weight = np.random.uniform(-1, 1, 3)
    # binaryOrPerceptron = perceptron(listBinaryOR, weight, 100, 0.2, binary, 0)
    # print('weight: ', binaryOrPerceptron[0])
    # print('result: ', binaryOrPerceptron[1])
    # print('epochs: ', binaryOrPerceptron[2])
    # weight = np.random.uniform(-1, 1, 3)
    # bipolarOrPerceptron = perceptron(listBipolarOR, weight, 100, 0.2, bipolar, 0)
    # print('weight: ', bipolarOrPerceptron[0])
    # print('result: ', bipolarOrPerceptron[1])
    # print('epochs: ', bipolarOrPerceptron[2])
    #
    # print()
    # print('Perceptron AND:')
    # weight = np.random.uniform(-1, 1, 3)
    # binaryAndPerceptron = perceptron(listBinaryAND, weight, 100, 0.2, binary, 0)
    # print('weight: ', binaryAndPerceptron[0])
    # print('result: ', binaryAndPerceptron[1])
    # print('epochs: ', binaryAndPerceptron[2])
    # weight = np.random.uniform(-1, 1, 3)
    # bipolarAndPerceptron = perceptron(listBipolarAND, weight, 100, 0.2, bipolar, 0)
    # print('weight: ', bipolarAndPerceptron[0])
    # print('result: ', bipolarAndPerceptron[1])
    # print('epochs: ', bipolarAndPerceptron[2])
    #
    # print()
    # print('Adaline OR: ')
    # weight = np.random.uniform(-1, 1, 3)
    # adalineOr = adaline(listBipolarOR, weight, 100, 0.2, bipolar)
    # print('weight: ', adalineOr[0])
    # print('result: ', adalineOr[1])
    # print('epochs: ', adalineOr[2])
    #
    # print()
    # print('Adaline AND:')
    # weight = np.random.uniform(-1, 1, 3)
    # adalineAnd = adaline(listBipolarAND, weight, 100, 0.2, bipolar)
    # print('weight: ', adalineAnd[0])
    # print('result: ', adalineAnd[1])
    # print('epochs: ', adalineAnd[2])

    listBinaryOR = list_binary_or_generate()
    listBipolarOR = list_bipolar_or_generate()
    listBinaryAND = list_binary_and_generate()
    listBipolarAND = list_bipolar_and_generate()

    const_alpha()

    const_weight()

    change_activ_acepptable()
